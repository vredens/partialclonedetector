package PartialCloneDetector::Node;

use Data::Dumper;

my $id = 0;

sub new {
	my $class = shift;
	$id++;
	my $self = {
		'id'      => $id,
		'data'    => shift,
		'child'   => undef,
		'parent'  => undef,
		'sibling' => undef
	};
	
	bless $self, $class;
}

sub id {
	my $self = shift;
	
	return $self->{'id'};
}

sub _sibling {
	my $self = shift;
	my $_sib = shift;
	
	$self->{'sibling'} = $_sib if defined $_sib;
	
	return $self->{'sibling'};
}

sub sibling {
	my $self = shift;
	
	my $_sib = shift;
	if (defined $_sib and ref $_sib eq ref $self) {
		# set the same parent
		$_sib->parent($self->parent());
		# inform the sibling of other siblings
		$_sib->_sibling($self->{'sibling'}) if (defined $self->{'sibling'});
		# make it my sibling
		$self->{'sibling'} = $_sib;
	}
	defined $self->{'sibling'} ? $self->{'sibling'} : undef;
}

# return the first child or add a child
sub child {
	my $self = shift;
	my $_child = shift;
	if (defined $_child and ref $_child eq ref $self) {
		if (defined $self->{'child'}) {
			# we let the first child's sibling method take care of everything
			$self->{'child'}->sibling($_child);
		} else {
			# set me as the parent
			$_child->parent($self);
			# make it my child
			$self->{'child'} = $_child;
		}
	}
	defined $self->{'child'} ? $self->{'child'} : undef;
}

sub parent {
	my $self = shift;

	# if we received a parent then we set it
	my $_parent = shift;
	$self->{'parent'} = $_parent if (defined $_parent and ref $_parent eq ref $self);

	return $self->{'parent'};
}

sub extra_data {
	my $self = shift;
	
	my $data = shift;
	$self->{'leaf-data'} = $data if defined $data;
	
	defined $self->{'leaf-data'} ? $self->{'leaf-data'} : undef;
}

sub data {
	my $self = shift;
	
	defined $self->{'data'} ? $self->{'data'} : 'undef';
}

sub is_leaf {
	my $self = shift;
	
	return not defined $self->{'child'};
}

sub is_root {
	my $self = shift;
	
	return not defined $self->{'parent'};
}

sub match_data {
	my $self = shift;
	my $data = shift;
	my $partial = shift;
	
	$partial = 0 unless defined $partial;
	
	return 1 if not defined $self->{'data'} and not defined $data; # undef == undef : true
	return $partial unless defined $self->{'data'}; # any == undef : true
	return 0 unless defined $data; # undef == any : false
	return 1 if $self->{'data'} eq $data; # one == one
	return 0;
}

sub children_have_data {
	my $self = shift;
	my $child_data = shift;
	
	return 0 unless defined $self->{'child'};
	
	my $c = $self->{'child'};
	do {
		#print "\t", "comparing ", $c->data(), ' to ', $child_data, "\n";
		return 1 if $c->match_data($child_data);
		$c = $c->sibling();
	} while (defined $c);
	return 0;
}

sub dump {
	my $self = shift;
	my $s = "[node: " . $self->{'id'} . "]";
	$s .= "[parent: " . $self->parent()->id() . "]" if defined $self->parent();
	$s .= "[data: " . $self->data() . "]";
	return $s;
}

1;

__END__

