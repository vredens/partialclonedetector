package PartialCloneDetector;

use strict;
use warnings;

use Data::Dumper;
use Carp;

use PartialCloneDetector::Node;

# used only for debugging
#use EerieSoft::RecThis qw/rec_this rec_pstart rec_pend/;

our $VERSION = '0.1';

sub new {
	my $class = shift;
	
	my $self = {
		'root' => new PartialCloneDetector::Node('root'),
		'id-field' => shift,
		'fields' => \@_,
		'originals' => [],
		'clones' => []
	};
	
	croak "You need to pass at least one field" unless @{$self->{'fields'}} > 0;
	
	bless $self, $class;
}

sub originals {
	my $self = shift;
	
	return $self->{'originals'};
}

sub clones {
	my $self = shift;
	
	return $self->{'clones'};
}

sub print_entity {
	my $e = shift;
	
	print "entity: ";
	foreach (keys %{$e}) {
		print '[', $_, ':', $e->{$_}, ']';
	}
	print "\n";
}

sub _parse_entry_field {
	my $self = shift;
	my $e = shift;
	my $node  = shift;
	my $field = shift; # strip one of the entry/record parameters
	
	my $rec_id = $e->{$self->{'id-field'}};
	my $rec_cur_field = defined $e->{$field} ? $e->{$field} : undef;
	my $rec_nxt_field = defined $e->{$_[0]} ? $e->{$_[0]} : undef if @_ > 0;
	
	while (defined $node) {
		if ($node->match_data($rec_cur_field, 0)) { # no partial matches, that's what the 0 stands for
			if (@_ > 0) {
				unless (defined $node->child()) {
					my $nn = PartialCloneDetector::Node->new($rec_nxt_field);
					$node->child($nn);
					
					# rec_this(5, 'created child ', $nn->dump(), ' for node ', $node->dump());
				}
				$self->_parse_entry_field($e, $node->child(), @_);
			} else {
				unless ($node->children_have_data($rec_id)) {
					my $nn = PartialCloneDetector::Node->new($rec_id);
					$nn->extra_data($e);
					$node->child($nn);
					
					push @{$self->{'leafs'}}, $nn;

					# rec_this(5, 'created leaf ', $nn->dump(), ' for node ', $node->dump());
				}
			}
			$node = undef;
		} else {
			unless (defined $node->sibling()) {
				my $nn = PartialCloneDetector::Node->new($rec_cur_field);
				$node->sibling($nn);

				# rec_this(5, 'added first sibling ', $nn->dump(), ' to node ', $node->dump());
			} else {
				if (defined $rec_cur_field and $node->sibling()->data() eq 'undef' and not $node->parent()->children_have_data($rec_cur_field)) {
					my $nn = PartialCloneDetector::Node->new($rec_cur_field);
					$node->sibling($nn);
					
					# rec_this(5, 'added sibling ', $nn->dump(), ' to node ', $node->dump());
				}
			}
			$node = $node->sibling();
		}
	};
}

sub _parse_leaf_entry_field {
	my $self = shift;
	my $e = shift;
	my $node  = shift;
	my $field = shift;
	
	my $rec_id = $e->{$self->{'id-field'}};
	while (defined $node) {
		if ($node->match_data($e->{$field}, 1)) { # we are rematching nodes, so partial matches are allowed
			if (@_ > 0) {
				$self->_parse_leaf_entry_field($e, $node->child(), @_);
			} elsif (defined $node->child()) {
				unless ($node->children_have_data($rec_id)) {
					my $nn = PartialCloneDetector::Node->new($rec_id);
					$nn->extra_data($e);
					$node->child($nn);
				}
			}
		}
		$node = $node->sibling();
	}
}

sub process {
	my $self = shift;
	my $data = shift;
	
	croak "Data must be an array ref with entries" unless ref $data eq 'ARRAY';
	
	#rec_pstart('init');
	foreach my $entry (@{$data}) {
		unless (defined $self->{'root'}->child()) {
			my $p = PartialCloneDetector::Node->new($entry->{$self->{'fields'}->[0]});
			$self->{'root'}->child($p);
		}
		$self->_parse_entry_field($entry, $self->{'root'}->child(), @{$self->{'fields'}});
	}	
	#rec_pend('init', 'initial data parsing');

	#$self->dump('tree'); # debug
}


sub analyze {
	my $self = shift;
	
	my @_leafs = @{$self->{'leafs'}};

	# finalize the tree
	#rec_pstart('final');
	foreach my $leaf (@_leafs) {
		$self->_parse_leaf_entry_field($leaf->extra_data(), $self->{'root'}->child(), @{$self->{'fields'}});
	}
	#rec_pend('final', 're-introduce leafs into the tree to generate clone candidates');
	
	#rec_pstart('final');
	foreach my $leaf (@_leafs) {
		if (defined $leaf->sibling() or $leaf->parent->child->id() != $leaf->id) {
			my $clone = {
				'clone' => $leaf,
				'possible-originals' => []
			};
			my $node = $leaf->parent->child();
			do {
				push @{$clone->{'possible-originals'}}, $node if $node->id != $leaf->id;
				$node = $node->sibling() if defined $node->sibling();
			} while (defined $node->sibling());
			push @{$self->{'clones'}}, $clone;
		} else {
			push @{$self->{'originals'}}, $leaf;
		}
	}
	#rec_pend('final', 'clone and originals detection');

	#$self->dump('retree'); # debug
}

# tree traverse using depth-first
sub tree_traverse_depth_first {
	my $node = shift;
	croak "you need to pass a PartialCloneDetector::Node blessed data" unless ref $node eq 'PartialCloneDetector::Node';
	
	return $node->child() if defined $node->child();
	return $node->sibling() if defined $node->sibling();
	unless ($node->is_root()) {
		do {
			$node = $node->parent();
			# rec_this(5, 'visiting node ', $node->id());
		} while (not $node->is_root() and not defined $node->sibling());
		if ($node->is_root()) {
			# rec_this(5, 'got all the way back to root, terminating with undef');
			return undef;
		}
		# rec_this(5, 'returning ancestor sibling node');
		return $node->sibling;
	}
	
	rec_this(1, 'tree traverse should never reach here, something is wrong with the tree structure');
	return undef; # safety, should never reach here.
}

sub dump {
	my $self = shift;
	my $name = shift;

	my $node = $self->{'root'};

	open (my $fh, ">", "$name.dot");
	print $fh "digraph tree {\n";
	
	our @arcs = ();
	our $nodes = {};
	
	sub tree_traverse_rec {
		my $node = shift;
		if (defined $node) {
			# label for the parent
			if (defined $node->parent() and ! defined $nodes->{$node->parent()->id()}) {
				$nodes->{$node->parent()->id()} = $nodes->parent()->data() . ':' . $node->parent()->id();
			}
			# label for the child
			if (! defined $nodes->{$node->id()}) {
				$nodes->{$node->id()} = $node->data() . ':' . $node->id();
			}
			# add the arc between son and father
			if (defined $node->parent()) {
				push @arcs, [$node->parent()->id(), $node->id()];
			}
			
			# check siblings
			if ($node->sibling()) {
				tree_traverse_rec($node->sibling());
			}
			
			# check children
			if ($node->child()) {
				tree_traverse_rec($node->child());
			}
		}
	}

	tree_traverse_rec($node);
	
	foreach (keys %{$nodes}) {
		print $fh $_, ' [label="', $nodes->{$_}, "\"];\n";
	}
	foreach (@arcs) {
		print $fh $_->[0], ' -> ', $_->[1], ";\n";
	}
	
	print $fh "}\n";
	close ($fh);
}

1;

__END__

=head1 NAME

PartialCloneDetector

=head1 SYNOPSIS

	use PartialCloneDetector;
	
	# sample data
	my $data = [
		{'id' => 1, 'first_name' => 'john', 'last_name' => 'smith', 'age' => 30, 'sex' => 'male'},
		{'id' => 1, 'first_name' => 'john', 'last_name' => undef, 'age' => undef, 'sex' => undef},
		{'id' => 1, 'first_name' => 'john', 'last_name' => undef, 'age' => 27, 'sex' => undef},
	];
	
	# this should always be your init sequence. methods are separated so you can
	# generate dumps of the initial tree also
	my $pcd = PartialCloneDetector->new(qw/id first_name last_name age sex/);
	$pcd->process($data);
	$pcd->analyze();
	
	# returns a list of original entities
	my $originals = $pcd->originals();
	
	# returns a list of potential clones and for each clone the list of
	# potential originals
	my $clones = $pcd->clones();
	
	# generates a dot file with the given name and the tree structure
	$pcd->dump('my-dot-file'); 

=head1 DESCRIPTION

PartialCloneDetector is a small module for implementing a partial clone
detector algorythm.

Partial Clones are best described as copies of data records but with
only partial information. For example, let's consider a correct record
of a Person with first name 'John', last name 'Smith', age '30' and sex
'Male'. A partial clone of John would be 'John', last name unknown, age 
unknown, sex 'Male'.

This library returns a list of non-clones and partial clones with a list
of possible originals.

=head1 METHODS

=head2 new ($id_field, @fields)

Creates a new PartialCloneDetector. The parameters are all names of
attributes of the entities which will be passed to the process method.

The $id_field is the field which identifies a given entity

The @fields are all fields which should be part of the cloning analyzis,
i.e., the fields which will be used to compare clones.

Returns a ready to use clone detector.

=head2 process ($data_arr_ref)

Processes the data into the initial tree.

=head2 analyze ()

Processes the initial tree and generates the potential clones.

=head2 originals ()

Returns the list of original entities. This method should be run after running
analyze.

=head2 clones ()

Returns the list of clones. This method should be run after running analyze.

=head2 dump ($file_base_name)

Generates a "$file_base_name.dot" file with a Graphviz .dot notation so you can
generate a graph out of it.

=head1 SEE ALSO

Graphviz Website at http://www.graphviz.org/

=head1 AUTHOR

J.B. Ribeiro, E<lt>jbr at eerieguide dot comE<gt>

=head1 LICENSE

Copyright (C) 2013 by J.B. Ribeiro

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.12.3 or,
at your options, any later version of Perl 5 you may have available.

=cut
